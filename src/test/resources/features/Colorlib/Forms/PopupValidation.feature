#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Formulario Popup Validation
    El usuario debe poder ingresar al  formulario los datos requeridos.
    Cada campo del formulario realiza validaciones de obligatoriedad,
    longitud y formato, el sistema debe presentar las validaciones respectivas
    para cada campo a través de un globo informativo.
    
  @CasoExitoso
  Scenario: Diligenciamiento exitoso del formulario Popup Validation,
  no se presenta ningún mensaje de validación.
  
    Given Autentico en colorlib con usuario "demo" y clave "demo"
    
    And Ingreso a la funcionalidad Forms Validation
    
    When Diligencio Formulario PopupValidation
    
    	| Required |Select | MultipleS1 | MultipleS2 | Url                   | Email            | Password1 | Password2 | MinSize | MaxSize | Number | IP          | Date       | DateEarlier |
    	| Valor1   |Golf   | Tennis     | Golf       | http://www.valor1.com | valor1@gmail.com | valor1    | valor1    | 123456  | 987654  | -98.76 | 231.252.5.4 | 2018-07-24 | 2012/09/12  |
    	
    Then Verifico ingreso exitoso
   
     @CasoAlterno
  Scenario: Diligenciamiento exitoso del formulario Popup Validation,
  no se presenta ningún mensaje de validación.
  
    Given Autentico en colorlib con usuario "demo" y clave "demo"
    
    And Ingreso a la funcionalidad Forms Validation
    
    When Diligencio Formulario PopupValidation
    
    	| Required |Select | MultipleS1 | MultipleS2 | Url                   | Email            | Password1 | Password2 | MinSize | MaxSize | Number | IP          | Date       | DateEarlier |
    	|    			 |Golf   | Tennis     | Golf       | http://www.valor1.com | valor1@gmail.com | valor1    | valor1    | 123456  | 987654  | -98.76 | 231.252.5.4 | 2018-07-24 | 2012/09/12  |
    	| Valor1   |Choose a sport | Tennis     | Golf       | http://www.valor1.com | valor1@gmail.com | valor1    | valor1    | 123456  | 987654  | -98.76 | 231.252.5.4 | 2018-07-24 | 2012/09/12  |
    	| Valor1   |Golf   | Choose a sport     | Choose a sport       | http://www.valor1.com | valor1@gmail.com | valor1    | valor1    | 123456  | 987654  | -98.76 | 231.252.5.4 | 2018-07-24 | 2012/09/12  |
     	| Valor1   |Golf   |Tennis |Golf | valor1.com | valor1@gmail.com | valor1    | valor1    | 123456  | 987654  | -98.76 | 231.252.5.4 | 2018-07-24 | 2012/09/12  |
     	| Valor1   |Golf   |Tennis |Golf | http://www.valor1.com | valor1gmail.com | valor1    | valor1    | 123456  | 987654  | -98.76 | 231.252.5.4 | 2018-07-24 | 2012/09/12  |
     	| Valor1   |Golf   | Tennis     | Golf       | http://www.valor1.com | valor1@gmail.com |     | valor1    | 123456  | 987654  | -98.76 | 231.252.5.4 | 2018-07-24 | 2012/09/12  |
     	| Valor1   |Golf   | Tennis     | Golf       | http://www.valor1.com | valor1@gmail.com |  valor1   | valor321    | 123456  | 987654  | -98.76 | 231.252.5.4 | 2018-07-24 | 2012/09/12  |
     	| Valor1   |Golf   | Tennis     | Golf       | http://www.valor1.com | valor1@gmail.com | valor1    | valor1    | 12345  | 987654  | -98.76 | 231.252.5.4 | 2018-07-24 | 2012/09/12  |
     	| Valor1   |Golf   | Tennis     | Golf       | http://www.valor1.com | valor1@gmail.com | valor1    | valor1    | 123456  | 9876543  | -98.76 | 231.252.5.4 | 2018-07-24 | 2012/09/12  |
     	| Valor1   |Golf   | Tennis     | Golf       | http://www.valor1.com | valor1@gmail.com | valor1    | valor1    | 123456  | 987654  | -A98.76 | 231.252.5.4 | 2018-07-24 | 2012/09/12  |
     	| Valor1   |Golf   | Tennis     | Golf       | http://www.valor1.com | valor1@gmail.com | valor1    | valor1    | 123456  | 987654  | -98.76 | 231.252.5 | 2018-07-24 | 2012/09/12  |
     	| Valor1   |Golf   | Tennis     | Golf       | http://www.valor1.com | valor1@gmail.com | valor1    | valor1    | 123456  | 987654  | -98.76 | 231.252.5.4 | 2018-13-26 | 2012/09/12  |
     	| Valor1   |Golf   | Tennis     | Golf       | http://www.valor1.com | valor1@gmail.com | valor1    | valor1    | 123456  | 987654  | -98.76 | 231.252.5.4 | 2018-07-24 | 2012/09/14  |
    	
    Then Verificar que se presente Globo Informativo de validación.
    
    
@CasoFeliz
  Scenario Outline: Consultar tabla de clientes CNAME y verificar resultados
  
    Given Consultar CNAME
			| <Documento>		| <Tipo Docto>		| <Vombre>		| <Control Terceros>	|
			
Examples:
			| Documento					| Tipo Docto		| Nombre					| Control Terceros		|
			| 000008000003931		|3							| PERFORMANCE			| 8										|
			| 000008000004576		|3							| ELIANA MORALES	| 1										|

    
    
    