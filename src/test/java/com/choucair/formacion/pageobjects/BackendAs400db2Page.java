package com.choucair.formacion.pageobjects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import com.choucair.formacion.utilities.Sql_Execute;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class BackendAs400db2Page {
	
	
	public String Armar_Query_Consulta_CNAME(String strDocumento) {
		
		String strQuery = "SELECT 	* FROM GPLILIBRA.TMPCNAME WHERE CNNOSS ='<DOCUMENTO>'";
		strQuery = strQuery.replace("<documento>", strDocumento);
		return strQuery;
		
	}
	
	
	public ResultSet Ejecutar_Query(String Query) throws SQLException {
		
		Sql_Execute DAO = new Sql_Execute();
		ResultSet rs = DAO.sql_Execute(Query);
		return rs;
		
	}
	
	
	public void Verificar_Consulta_CNAME(ResultSet rs, List<List<String>> data) throws SQLException{
		
		while(rs.next()) {
			
			String Documento_Recibido = rs.getString(1);
			String Documento_Esperado = data.get(0).get(0);
			assertThat(Documento_Recibido, equalTo(Documento_Esperado));
			String TipoDoc_Recibido = rs.getString(2);
			String TipoDoc_Esperado = data.get(0).get(1);
			assertThat(TipoDoc_Recibido, equalTo(Documento_Esperado));
			String Nombre_Recibido = rs.getString(3);
			String Nombre_Esperado = data.get(0).get(2);
			assertThat(Nombre_Recibido.trim(), equalTo(Nombre_Esperado.trim()));
			String CtrlTercero_Recibido = rs.getString(4);
			String CtrlTercero_Esperado = data.get(0).get(3);
			assertThat(CtrlTercero_Recibido.trim(), equalTo(CtrlTercero_Esperado.trim()));
			
			
			
		}
		
	}
	

}
