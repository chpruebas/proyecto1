package com.choucair.formacion.definition;

import com.choucair.formacion.steps.BackendAs400db2Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class BackendAs400db2Definition {
	
	
	@Steps
	BackendAs400db2Steps backend400db2Steps;
	
	@Given("^Consultar CNAME$")
	public void consultar_CNAME(DataTable dtDatosPrueba) throws Throwable {
	    List<List<String>> data = dtDatosPrueba.raw();
	    backend400db2Steps.Consultar_CNAME(data);
	}
	

}
