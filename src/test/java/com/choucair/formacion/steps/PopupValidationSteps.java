package com.choucair.formacion.steps;

//import java.util.List;

import com.choucair.formacion.pageobjects.colorlibLoginPage;

import com.choucair.formacion.pageobjects.ColorlibMenuPage;

import net.thucydides.core.annotations.Step;
//import cucumber.api.DataTable;

public class PopupValidationSteps {
	
	colorlibLoginPage colorlibLoginPage;
	ColorlibMenuPage colorlibMenuPage;


	@Step
	public void login_colorlib(String strUsuario, String strPass) {
		
		colorlibLoginPage.open();
		colorlibLoginPage.IngresarDatos(strUsuario, strPass);
		colorlibLoginPage.VerificaHome();
		
	}
	
	@Step
	public void ingresar_form_validation() {
		
		colorlibMenuPage.menuFormValidation();
				
	}
	
	
		
}
